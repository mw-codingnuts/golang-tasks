package main

type cookie struct {
	flavor   string
	number   int
	producer string
}

var flavors = []string{"chocolate", "nuts", "lemon", "vanilla"}

func main() {
}

func cookieBakery(cookieBelt chan<- cookie, producer string, stop <-chan bool) {
}

func cookieMonster(cookieBelt <-chan cookie, stop <-chan bool) {
}
