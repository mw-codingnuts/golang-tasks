package main

import "sync"

type cookie struct {
	flavor   string
	number   int
	producer string
}

var flavors = []string{"chocolate", "nuts", "lemon", "vanilla"}

func main() {
}

func cookieBakery(cookieBelt chan<- cookie, producer string, stop <-chan bool, wg *sync.WaitGroup) {
}

func cookieMonster(cookieBelt <-chan cookie, wg *sync.WaitGroup) {
}
