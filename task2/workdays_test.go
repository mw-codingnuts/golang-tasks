package main

import "testing"

func TestWorkdays(t *testing.T) {
	weekdays := []Weekday{Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday}
	workdays := []bool{true, true, true, true, true, false, false}
	for _, weekday := range weekdays {
		result := isWorkday(weekday)
		if result != workdays[weekday] {
			t.Fatalf("isWorkday(%d) != %t", weekday, workdays[weekday])
		}
	}
}
