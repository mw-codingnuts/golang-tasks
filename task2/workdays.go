package main

type Weekday int

const (
	Monday Weekday = iota
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

func isWorkday(weekday Weekday) bool {
	switch weekday {
	case Monday:
	case Tuesday:
	case Wednesday:
	case Thursday:
	case Friday:
		return true
	case Saturday:
	case Sunday:
		return false
	}
	return false
}
