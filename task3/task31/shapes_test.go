package main

import "testing"
import "math"

func TestRectangle(t *testing.T) {
	rectangle := Rectangle{15, 10}
	var area, expectedArea float64
	area = rectangle.Area()
	expectedArea = 150
	if area != expectedArea {
		t.Fatalf("Rectangle's area must be %f", expectedArea)
	}
}

func TestSquare(t *testing.T) {
	square := Square{10}
	var area, expectedArea float64
	area = square.Area()
	expectedArea = 100
	if area != expectedArea {
		t.Fatalf("Square's area must be %f", expectedArea)
	}
}

func TestCircle(t *testing.T) {
	circle := Circle{3}
	var area, expectedArea float64
	area = math.Round(circle.Area())
	expectedArea = 28
	if area != expectedArea {
		t.Fatalf("Circle's area must be %f", expectedArea)
	}
}
