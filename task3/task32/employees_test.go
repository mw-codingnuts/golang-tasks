package main

import "testing"
import "strings"

func testEmployee(t *testing.T) Employee {
	id := "11"
	firstName := "John"
	lastName := "Doe"
	businessDivision := "Software Testing"
	employee := Employee{id, firstName, lastName, businessDivision}
	if employee.ID != id {
		t.Fatalf("Employee's ID must be '%s'", id)
	}
	if employee.FirstName != firstName {
		t.Fatalf("Employee's first name must be '%s'", firstName)
	}
	if employee.LastName != lastName {
		t.Fatalf("Employee's last name must be '%s'", lastName)
	}
	if employee.BusinessDivision != businessDivision {
		t.Fatalf("Employee's business division must be '%s'", businessDivision)
	}
	return employee
}

func TestEmployee(t *testing.T) {
	testEmployee(t)
}

func TestEngineer(t *testing.T) {
	employee := testEmployee(t)
	programmingLanguages := []string{"Go", "Haskell", "C"}
	engineer := Engineer{employee, programmingLanguages}
	if len(engineer.ProgrammingLanguages) != len(programmingLanguages) {
		t.Fatalf("Engineers's programming languages must be %s", strings.Join(programmingLanguages, ","))
	}
}

func TestTester(t *testing.T) {
	employee := testEmployee(t)
	hasISTQBCertificate := true
	tester := Tester{employee, true}
	if tester.HasISTQBCertificate != hasISTQBCertificate {
		t.Fatalf("Tester must have an ISTQB certificate")
	}
}

func TestConsultant(t *testing.T) {
	employee := testEmployee(t)
	isScrumMaster := true
	consultant := Consultant{employee, true}
	if consultant.IsScrumMaster != isScrumMaster {
		t.Fatalf("Consultant must be a scrum master")
	}
}
