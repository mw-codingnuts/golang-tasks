package main

import "testing"

func TestFib(t *testing.T) {
	fibonacciNumbers := []int{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55}
	for n := 0; n < len(fibonacciNumbers); n++ {
		fibN := fib(n)
		if fibN != fibonacciNumbers[n] {
			t.Fatalf("fib(%d) != %d", n, fibonacciNumbers[n])
		}
	}
}
