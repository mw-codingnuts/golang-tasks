package main

import "fmt"
import "os"
import "strconv"

func fib(n int) int {
	// Add a meaningful implementation here
	// c.f. https://en.wikipedia.org/wiki/Fibonacci_number
	return 1
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s n\n", os.Args[0])
		os.Exit(1)
	}

	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Printf("Failed to convert to int: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("fib(%d) = %d\n", n, fib(n))
}
